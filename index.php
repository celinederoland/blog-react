<?php
/**
 * Created by PhpStorm.
 * User: celinederoland
 * Date: 14/05/16
 * Time: 06:25
 */

routeRequest();

function routeRequest()
{
    $cards = file_get_contents('cards.json');
    $uri = $_SERVER['REQUEST_URI'];
    if ($uri == '/') {
        echo file_get_contents('./public/dist/index.html');
    } elseif (preg_match('/\/api\/cards(\?.*)?/', $uri)) {
        if($_SERVER['REQUEST_METHOD'] === 'POST') {
            $cardsDecoded = json_decode($cards, true);
            $cardsDecoded[] = [
                'id'      => round(microtime(true) * 1000),
                'author'  => $_POST['author'],
                'text'    => $_POST['text']
            ];
            $cards = json_encode($cardsDecoded, JSON_PRETTY_PRINT);
            file_put_contents('cards.json', $cards);
        }
        header('Content-Type: application/json');
        header('Cache-Control: no-cache');
        header('Access-Control-Allow-Origin: *');
        echo $cards;
    }

    return false;
}