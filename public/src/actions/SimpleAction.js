/**
 * Created by celinederoland on 15/05/16.
 */
'use strict';
var AppDispatcher = require('../dispatcher/Dispatcher');
var SimpleConstants = require('../constants/SimpleConstants');

var SimpleActions = {

    /**
     */
    change: function () {
        AppDispatcher.dispatch({
            action: {
                action: SimpleConstants.SIMPLEITEM_CHANGE,
                data: null
            }
        });
    }

};

module.exports = SimpleActions;
