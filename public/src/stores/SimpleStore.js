/**
 * Created by celinederoland on 15/05/16.
 */
'use strict';
var AppDispatcher = require('../dispatcher/Dispatcher');
var EventEmitter = require('events').EventEmitter;
var SimpleConstants = require('../constants/SimpleConstants');
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var _currentValue = {value: 42}; // The Model

/**
 * Modify a value.
 */
function changeValue() {
    // Using the current timestamp in place of a real id.
    _currentValue = {value: Date.now()};
}

var SimpleStore = assign({}, EventEmitter.prototype, {

    /**
     * Get the value.
     * @return {object}
     */
    getValue: function () {
        return _currentValue;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    dispatcherIndex: AppDispatcher.register(function (payload) {
        var action = payload.action;
        switch (action.action) {
            case SimpleConstants.SIMPLEITEM_CHANGE:
                changeValue();
                SimpleStore.emitChange();
                break;

            // add more cases for other actionTypes, like SIMPLEITEM_UPDATE, etc.
        }

        return true; // No errors. Needed by promise in Dispatcher.
    })

});

module.exports = SimpleStore;