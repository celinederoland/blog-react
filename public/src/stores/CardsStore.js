/**
 * Created by celinederoland on 15/05/16.
 */
'use strict';
var AppDispatcher = require('../dispatcher/Dispatcher');
var EventEmitter = require('events').EventEmitter;
var CardsConstants = require('../constants/CardsConstants');
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var _currentValue = [
    {
        "id": 1388534400000,
        "title": "Do this",
        "text": "Hey there!"
    },
    {
        "id": 1420070400000,
        "title": "Do that",
        "text": "React is *great*!"
    }
]; // The Model

/**
 * Modify a value.
 */
function changeValue() {
    // Using the current timestamp in place of a real id.
    _currentValue = [
        {
            "id": 1388534400000,
            "title": "Do this " + Date.now(),
            "text": "Hey there!"
        },
        {
            "id": 1420070400000,
            "title": "Do that " + Date.now(),
            "text": "React is *great*!"
        }
    ];
}

var CardsStore = assign({}, EventEmitter.prototype, {

    /**
     * Get the value.
     * @return {object}
     */
    getAll: function () {
        return _currentValue;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    dispatcherIndex: AppDispatcher.register(function (payload) {
        var action = payload.action;
        switch (action.action) {
            case CardsConstants.SIMPLEITEM_CHANGE:
                changeValue();
                CardsStore.emitChange();
                break;

            // add more cases for other actionTypes, like SIMPLEITEM_UPDATE, etc.
        }

        return true; // No errors. Needed by promise in Dispatcher.
    })

});

module.exports = CardsStore;