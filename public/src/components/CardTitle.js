'use strict';
var React = require('react');

/**
 * A title in the cards menu, allowing user to select this card
 */
var CardTitle = React.createClass({

    selectCard: function () {

        this.props.onSelectCard(this.props.card.id);
    },

    render: function () {

        return (
            <li onClick={this.selectCard}>{ this.props.card.title }</li>
        );
    }
});

module.exports = CardTitle;