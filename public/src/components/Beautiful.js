/**
 * Created by celinederoland on 15/05/16.
 */
'use strict';
var React = require('react');
var SimpleStore = require('../stores/SimpleStore');
var SimpleAction = require('../actions/SimpleAction');

var BeautifulComponent = React.createClass({

    componentDidMount: function () {
        SimpleStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        SimpleStore.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState(SimpleStore.getValue());
    },

    getInitialState: function () {
        return SimpleStore.getValue();
    },

    _onChangeClick: function () {
        SimpleAction.change();
    },

    render: function () {
        return (
            <div
                className="alert-success"
                onClick={this._onChangeClick}
            >
                This is beautiful component {this.state.value}
            </div>
        );
    }
});

module.exports = BeautifulComponent;