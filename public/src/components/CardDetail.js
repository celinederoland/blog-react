'use strict';
var React = require('react');

/**
 * Display all details about one card
 */
var CardDetail = React.createClass({

    render: function () {
        return (
            <div>
                <h2 className="card-title">
                    {this.props.title}
                </h2>
                <p>
                    {this.props.text}
                </p>
            </div>
        );
    }
});

module.exports = CardDetail;