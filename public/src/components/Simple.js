/**
 * Created by celinederoland on 15/05/16.
 */
'use strict';
var React = require('react');
var SimpleStore = require('../stores/SimpleStore');

var SimpleComponent = React.createClass({

    componentDidMount: function () {
        SimpleStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        SimpleStore.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState(SimpleStore.getValue());
    },

    getInitialState: function () {
        return SimpleStore.getValue();
    },

    render: function () {
        return (<strong> This is simple component {this.state.value} </strong>);
    }
});

module.exports = SimpleComponent;