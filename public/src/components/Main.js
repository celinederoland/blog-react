'use strict';
var React = require('react');
var CardsStore = require('../stores/CardsStore');
var CardList = require('./CardList');
var CardDetail = require('./CardDetail');

/**
 * The main component
 */

/**
 * React main component
 */
var ItPassionApp = React.createClass({

    /**
     * My state is defined by the card I'm currently displaying
     * @returns {{cardId: number}}
     */
    getInitialState: function () {

        return {
            cards: CardsStore.getAll(),
            cardId: 1420070400000
        };
    },

    /**
     * Switch to another card
     * @param id
     */
    switchCurrentCard: function (id) {

        this.setState({cardId: id});
    },

    render: function () {

        // Chose the card to display (according to my state)
        var self = this;
        var cardDetail = this.state.cards.map(function (card) {

            if (card.id === self.state.cardId) {
                return (
                    <CardDetail title={card.title} text={card.text} key={card.id}/>
                );
            }
        });

        // Render
        return (
            <div>
                <header className="header">
                    <h1>ItPassion</h1>
                </header>
                <section>
                    <CardList cards={this.state.cards} onSelectCard={this.switchCurrentCard}/>
                    { cardDetail }
                </section>
            </div>
        );
    }
});

module.exports = ItPassionApp;