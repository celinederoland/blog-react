'use strict';
var React = require('react');
var CardTitle = require('./CardTitle');

/**
 * The menu to display all cards and then select one to display
 */
var CardList = React.createClass({

    render: function () {

        // Loop over all cards to display it's title, allowing user to select one
        var self = this;
        var cards = this.props.cards.map(function (card) {

            return (
                <CardTitle onSelectCard={self.props.onSelectCard} card={card}/>
            );
        });

        return (
            <ul>
                { cards }
            </ul>
        );
    }
});

module.exports = CardList;