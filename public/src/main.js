$ = jQuery = require('jquery');

var SimpleComponent = require('./components/Simple');
var BeautifulComponent = require('./components/Beautiful');
var React = require('react');
var ItPassionApp = require('./components/Main');

React.render(
    <div>
        <SimpleComponent />
        <BeautifulComponent />
        <ItPassionApp />
    </div>,
    document.getElementById('simple-component')
);

