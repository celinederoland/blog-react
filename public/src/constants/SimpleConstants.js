/**
 * Created by celinederoland on 15/05/16.
 */

var keyMirror = require('keymirror');

module.exports = keyMirror({
    SIMPLEITEM_CHANGE: null
});