/**
 * Created by celinederoland on 14/05/16.
 */

var keyMirror = require('keymirror');

module.exports = keyMirror({
    CARD_CREATE: null,
    CARD_DESTROY: null
});